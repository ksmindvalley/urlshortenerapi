var httpResponse = require('../helpers/httpResponse');
var util = require('../helpers/util');
var token = require('../helpers/accesstoken');

module.exports = {
    jwtAuth: jwtAuth
}

function jwtAuth(req, res, next){
    var allowUrl = util.config.allowSkipJwtUrl;
    var needAuth = true;

    for(var i = 0; i < allowUrl.length; i++){
        if(allowUrl[i].url.substring(allowUrl[i].url.length - 2) == "/*"){
            if(req.url.indexOf(allowUrl[i].url.substring(0, allowUrl[i].url.length - 2)) >= 0 && allowUrl[i].method.indexOf(req.method.toLowerCase()) >= 0){
                needAuth = false;
                break;
            }
        }
        else{
            var url = req.url;

            if(url.substring(url.length - 1) == "/"){
                url = url.substring(0, url.length - 1);
            }

            if(url == allowUrl[i].url && allowUrl[i].method.indexOf(req.method.toLowerCase()) >= 0){
                needAuth = false;
                break;
            }
        }
    }

    if(needAuth){
        if(req.headers.authorization != undefined){
            token.jwtDecrypt(req.headers.authorization.replace("Bearer ", "").trim(), function(tokenKeyword){
                next();
            }, function(){
                httpResponse.resp401(res, next);
            });
        }
        else{
            httpResponse.resp401(res, next);
        }
    }
    else{
        next();
    }
}