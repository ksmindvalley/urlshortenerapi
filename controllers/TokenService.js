'use strict';

var util = require('../helpers/util');
var token = require('../helpers/accesstoken');

module.exports = {
    authentication: authentication
};

function authentication(keyword, callback) {
    if(keyword == util.config.tokenKeyword){
        callback(true, token.jwtEncrypt(keyword));
    }
    else{
        callback(false, "");
    }
}

