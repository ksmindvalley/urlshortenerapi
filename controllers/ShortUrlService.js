'use strict';

var shortUrlDao = require('../dao/shortUrlDao');

module.exports = {
    findByKey: findByKey,
    save: save
};

function findByKey(key, callback) {
    shortUrlDao.getDB();
    shortUrlDao.findByKey(key, function(error, result){
        callback(error, result);
    });
}

function save(obj, callback) {
    shortUrlDao.getDB();
    shortUrlDao.save(obj, function(error){
        callback(error);
    });
}
