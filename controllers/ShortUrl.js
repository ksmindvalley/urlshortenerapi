'use strict';

var httpResponse = require('../helpers/httpResponse');
var shortUrlService = require('./ShortUrlService');

module.exports = {
    shortUrlGetOne: shortUrlGetOne,
    shortUrlSave: shortUrlSave
};

function shortUrlGetOne(req, res, next){
    var key = req.swagger.params.key.value;
    
    shortUrlService.findByKey(key, function(error, result){
        if (error || result == null) {
            httpResponse.resp404(res, next, "URL not found");
        }
        else{
            var returnResult = {
                url: result.url
            }
            
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(returnResult || {}, null, 2));
        }
    });
}

function shortUrlSave(req, res, next){
    shortUrlService.save(req.swagger.params.body.value, function(error){
        if (error == 0) {
            httpResponse.resp501(res, next);
        }
        else if(error == 1){
            httpResponse.resp500(res, next);
        }
        else if(error == 2){
            var result = {
                msg: "Save successfully!"
            }
            
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(result || [], null, 2));
        }
    });
}

