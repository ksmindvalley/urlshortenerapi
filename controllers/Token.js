'use strict';

var httpResponse = require('../helpers/httpResponse');
var tokenService = require('./TokenService');

module.exports = {
    getToken: getToken
};

function getToken(req, res, next){
    tokenService.authentication(req.swagger.params.body.value.keyword, function(authenticated, token){
        if(authenticated){
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify({token: token} || {}, null, 2));
        }
        else{
            httpResponse.resp404(res, next, "Invalid keyword");
        }
    });
}