var jwt = require('jsonwebtoken');
var encryptionKey = "ilovemindvalley";

module.exports = {
    jwtEncrypt: function(keyword){
        var token = jwt.sign({keyword: keyword}, encryptionKey, {
            expiresIn: 300 //expires in seconds
        });

        return token;
    },
    jwtDecrypt: function(token, callback, error){
        jwt.verify(token, encryptionKey, function(err, decoded) {
            if(err){
                error();
            }

            if(decoded != undefined){
                callback(decoded);
            }
        });
    }
}