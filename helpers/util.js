var jsyaml = require('js-yaml');
var fs = require('fs');

module.exports = {
    config: jsyaml.safeLoad(fs.readFileSync('./api/config.yaml', 'utf8'))
}