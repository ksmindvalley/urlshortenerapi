var mongoose = require('mongoose');

module.exports = {
    getDB: function(){
        var conn = mongoose.createConnection("mongodb://localhost:27017/db1");
        var schema = new mongoose.Schema({},{strict: false, versionKey: false});
        this.dbConn = conn.model("shorturl", schema, "shorturl");
    },
    save: function(obj, callback){
        var self = this;
        
        this.findByKey(obj.name, function(err, results){            
            if(results != null){
                callback(0);
            }
            else{
                var objModel = self.dbConn(obj);
                
                objModel.save(function(err){
                    if(err){
                        callback(1);
                    }
                    else{
                        callback(2);
                    }
                });
            }
        });
    },
    findByKey: function(key, callback){
        this.dbConn.find({"name": key}, function(err, results) {
            if(results && results.length > 0){
                callback(err, results[0]);
            }
            else{
                callback(err, null);
            } 
        }).lean();
    }
}